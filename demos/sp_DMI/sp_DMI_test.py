import pytest
import torch
from magnumnp import *
from .run import run_DMI
import numpy as np
    
def test_DMI():    
    
    run_DMI()

    data = np.loadtxt("sp_DMI/data/m0_magnumnp.dat")
    ref = np.loadtxt("sp_DMI/ref/m_test.dat")
    
    data_x = torch.from_numpy(data[1:-1,1])
    data_z = torch.from_numpy(data[1:-1:2,3])
    ref_x = torch.from_numpy(ref[1:-1,1])
    ref_z = torch.from_numpy(ref[1:-1:2,3])
    
    torch.testing.assert_close(data_x, ref_x, atol=1e-10, rtol=1e-10) 
    torch.testing.assert_close(data_z, ref_z, atol=1e-10, rtol=1e-10)