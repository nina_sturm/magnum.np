import pytest
import torch
from magnumnp import *
from .run import run_softmagnetic_composite
import numpy as np
    
def test_softmagnetic_composite():    
    
    run_softmagnetic_composite()
    
    data = np.loadtxt("softmagnetic_composite/data/m.dat")
    ref = np.loadtxt("softmagnetic_composite/ref/m_test.dat")
    
    data_h = torch.from_numpy(data[:, 1])
    data_m = torch.from_numpy(data[:, 4])
    
    ref_h = torch.from_numpy(ref[:, 1])
    ref_m = torch.from_numpy(ref[:, 4])
    
    torch.testing.assert_close(data_h, ref_h, atol=1e-10, rtol=1e-10)
    torch.testing.assert_close(data_m, ref_m, atol=1e-10, rtol=1e-10)
    