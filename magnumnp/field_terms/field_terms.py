#
# This file is part of the magnum.np distribution
# (https://gitlab.com/magnum.np/magnum.np).
# Copyright (c) 2023 magnum.np team.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

from magnumnp.common import constants
import torch

__all__ = ["FieldTerm", "LinearFieldTerm"]

class FieldTerm(object):
    parameters = []

    def __init__(self, **kwargs):
        unknown_params = set(kwargs.keys()) - set(self.parameters)
        if unknown_params:
            raise Warning("Got unknown parameters '%s'. Ignoring!" % unknown_params)
        params = {key:key for key in self.parameters}
        params.update(kwargs)
        for key, value in params.items():
            setattr(self, key, value)

class LinearFieldTerm(FieldTerm):
    r"""
    
    This class is used when the field depends linearly on the magnetization m. In this case the linear field energy is given by:
    
    .. math::
        E^\text{lin} = - \frac{1}{2} \mu _0 V \sum_{i} M_s \vec{m_i} \cdot \vec{h_i}^\text{lin},
        
    where V = :math:`{\Delta}`x:math:`{\Delta}`y:math:`{\Delta}`z is the cell volume and $\vec{h}^\text{lin} is the corresponding field.
    """
    def E(self, state, domain = Ellipsis):
        E = -0.5 * constants.mu_0 * state.material["Ms"] * state.m * self.h(state) * state.cell_volumes
        return E[domain].sum()
