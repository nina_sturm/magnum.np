#
# This file is part of the magnum.np distribution
# (https://gitlab.com/magnum.np/magnum.np).
# Copyright (c) 2023 magnum.np team.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

mu_0 = 1.2566370614e-6
"""magnetic constant mu_0"""

gamma = 2.21276157e5
"""gyromagnetic ratio gamma"""

mu_B = 9.2740154e-24
"""Bohr magneton mu_bohr"""

e = 1.602176487e-19
"""elementary charge e"""

kb = 1.380648813e-23
"""Boltzmann constant kb"""

hbar = 1.0545718e-34
"""reduced Planck constant"""

me = 9.1093837e-31
"""Electron Mass"""

c = 299792458
"""Speed of Light"""
